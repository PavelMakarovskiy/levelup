package ru.levelUp.calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SimpleCalculator {
    private double firstValue;

    private double secondValue;

    private double result;

    private List<String> allowedActions = new ArrayList<>();

    {
        allowedActions.add("+");
        allowedActions.add("-");
        allowedActions.add("/");
        allowedActions.add("*");
        allowedActions.add("exit");
    }

    public static void main(String[] args) {
        SimpleCalculator calculator = new SimpleCalculator();
        System.out.println("Hello! You are using ru.levelUp.calculator.");
        while (true) {
            calculator.inputValues();
        }

    }

    public void inputValues() {
        System.out.println("Please input first value.");
        firstValue = checkInputValue(consoleReader());
        getListOfActions();
        String action = checkAction(consoleReader());
        System.out.println("Please input second value.");
        secondValue = checkInputValue(consoleReader());
        System.out.println("= " + calculation(firstValue, secondValue, action));
    }

    public String consoleReader() {
        String result = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            result = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public double calculation(double firstValue, double secondValue, String action) {
        if (action.equals("+")) {
            result = firstValue + secondValue;
        } else if (action.equals("-")) {
            result = firstValue - secondValue;
        } else if (action.equals("/")) {
            result = firstValue / secondValue;
        } else if (action.equals("*")) {
            result = firstValue * secondValue;
        } else {
            System.out.println("You inputted incorrect action. Please try again and enter valid action.");
            inputValues();
        }
        return result;
    }

    public void getListOfActions() {
        System.out.println("Please input action: \n" +
            "\"+\" for addition \n" +
            "\"-\" for deduction \n" +
            "\"/\" for division \n" +
            "\"*\" for multiply \n" +
            "\"exit\" to arrange exit from ru.levelUp.calculator");
    }

    public String checkAction(String action) {
        if (!allowedActions.contains(action)) {
            System.out.println("You inputed incorrect action. Please try again and enter valid action.");
            return checkAction(consoleReader());
        } else if (action.equals("exit")) {
            System.out.println("You exited ru.levelUp.calculator.");
            System.exit(0);
        }
        {
            return action;
        }
    }

    public double checkInputValue(String stringValue) {
        double value = 0;
        if (stringValue.equals("exit")) {
            System.out.println("You exited ru.levelUp.calculator.");
            System.exit(0);
        } else {
            try {
                value = Double.parseDouble(stringValue);
            } catch (NumberFormatException ex) {
                System.out.println("You inputted incorrect value. Please try again and enter valid value. " +
                    "Example of valid value: \"5\", \"6.7\"");
                checkInputValue(consoleReader());
            }
        }
        return value;
    }

}
